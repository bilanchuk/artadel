<?php

namespace App\Http\Controllers;

use App\Mail\NotifyMailer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class NotifyAttemptController extends Controller
{
    public function index(Request $request){
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
        ]);
        $name = $request->input('name');
        $email = $request->input('email');
        try {

            $toEmail = "t.klimovich@yahoo.com";

            $feedback = [
                'name' => $name,
                'email' => $email,
            ];
            Mail::to($toEmail)->send(new NotifyMailer($feedback));
            Mail::to("marketing.sofi@gmail.com")->send(new NotifyMailer($feedback));

        } catch (Exception $e) {
            return back()->with('swaled_status', json_encode([
                'title' => 'Notify attempt failed.'.$e,
                'text' => 'Please contact us through social links.',
                'icon' => 'error',
            ]));
        }

        return back()->with('swaled_status', json_encode([
            'title' => 'We\'ve got your message.',
            'text' => 'We wil return with response as soon as possible!',
            'icon' => 'success',
        ]));
    }
}
