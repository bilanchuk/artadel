<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('assets/style.css')}}">
    <title>Artadel</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-7 flex-wrap">
                <img src="{{asset('assets/logo.png
                ')}}" alt="" class="logo">
                <div class="title">Something Awesome is in the Work</div>
                <div class="subtitle">Wedding Dresses from Europe</div>
                <div class="text">Enhancing beauty and granting your <br>
                    wish to be one-of-a-kind on your special day</div>
                    <img src="{{asset('assets/line.png
                    ')}}" alt="">
                <div class="from-title">
                    NOTIFY ME WHEN IT'S READY
                </div>
                <div class="from-title" style="padding:10px; <?php if(\Session::has('swaled_status') || $errors->any()){
                    echo "display:block;";
                } else{
                    echo "display: none;";
                } ?>">
                     @if (\Session::has('swaled_status'))
                     <?php $status = json_decode(Session::get('swaled_status')); ?>
                     <div class="alert alert-{{$status->icon}}">
                         <ul>
                             <li>{!! $status->title !!}</li>
                             <li>{!! $status->text !!}</li>
                         </ul>
                     </div>
                 @endif
                 @if ($errors->any())
                     <div class="alert">
                         <ul>
                             @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                             @endforeach
                         </ul>
                     </div>
                 @endif
                </div>
                <div class="form-wrap">
                    <form action="/notify" method="POST">
                        @csrf
                        <input type="text" name="name"  placeholder="Name" class="form-text">
                        <input type="text" name="email"  placeholder="Email" class="form-text">
                        <input type="submit" value="NOTIFY ME" class="form-notify hvr-grow">
                    </form>
                </div>
                <div class="spam">We promise to never spam you</div>
                <div class="icons">
                    <a href="https://www.facebook.com/ArtadelBridal" target="_blank" class="hvr-shrink">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                    <a href="https://www.instagram.com/artadel_bridal/?hl=en" target="_blank" class="hvr-shrink">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="mailto:marketing.sofi@gmail.com;t.klimovich@yahoo.com" target="_blank" class="hvr-shrink">
                        <i class="fa fa-envelope-square"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
